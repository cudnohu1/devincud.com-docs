---
title: Rooted Boxes
author: Devin Cudnohufsky
date: 2021-05-19 00:00:00 +0500
categories: [Trophies]
tags: []
---

| Box                                                             |Source                   | Date       |
|-----------------------------------------------------------------|-------------------------|------------|
| Internal                                                        | Try Hack Me             | 2021-05-11 |
| Relevant                                                        | Try Hack Me             | 2021-05-09 |
| Overpass 2                                                      | Try Hack Me             | 2021-05-09 |
| Daily Bugle                                                     | Try Hack Me             | 2021-05-03 |
| Skynet                                                          | Try Hack Me             | 2021-05-02 |
| Game Zone                                                       | Try Hack Me             | 2021-05-02 |
| HackPark                                                        | Try Hack Me             | 2021-05-01 |
| Alfred                                                          | Try Hack Me             | 2021-05-01 |
| Steel Mountain                                                  | Try Hack Me             | 2021-04-29 |
| Kenobi                                                          | Try Hack Me             | 2021-04-28 |
| Blue                                                            | Try Hack Me             | 2021-04-27 |
| [Legacy]({% post_url write-ups/2021-05-02-hackthebox-legacy %}) | Hack The Box            | 2021-02-17 |
| [Lame]({% post_url write-ups/2021-05-02-hackthebox-lame %})     | Hack The Box            | 2021-02-16 |
| [Bashed]({% post_url write-ups/2021-05-02-hackthebox-bashed %}) | Hack The Box            | 2021-02-13 |
| Beta                                                            | OSCP / PWK Lab          | 2020-08-15 |
| Alpha                                                           | OSCP / PWK Lab          | 2020-08-15 |
| Break                                                           | OSCP / PWK Lab          | 2020-08-09 |
| fw_it                                                           | OSCP / PWK Lab          | 2020-08-08 |
| Pain                                                            | OSCP / PWK Lab          | 2020-07-30 |
| Dotty                                                           | OSCP / PWK Lab          | 2020-07-30 |
| Kioptrix Level 1                                                | VulnHub                 |            |
