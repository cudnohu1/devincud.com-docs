---
title: Password Cracking
author: Devin Cudnohufsky
date: 2020-08-20 00:00:00 +0500
categories: [Techniques]
tags: [hashcat]
---

## Password Mutations

`for i in $(cat <pw file>); do echo $i; echo ${i}<year>; done`
`hashcat --force --stdout <pw file> -r /usr/share/hashcat/rules/best64.rule`
`hashcat --force --stdout <pw file> -r /usr/share/hashcat/rules/best64.rule -r /usr/share/hashcat/rules/toggles1.rule`

### Mutation Ideas

1. Years
1. Months
1. Hostname
1. Season

## Password List Reductions

`cat <pw file> | sort -u`
`cat <pw file> | awk 'length($0) > <min length - 1>'`
`cat <pw file> | awk 'length($0) < <min length>'`