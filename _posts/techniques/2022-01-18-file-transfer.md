---
title: File Transfer
author: Devin Cudnohufsky
date: 2022-01-18 00:00:00 +0500
categories: [Techniques]
tags: [nc, powershell, socat]
---

## netcat

> Note: There will be no progress indicator, and the command will not terminate when complete. Estimate how long the transfer will take, terminate the command, and verify the file.

### Receive

`nc -nlvp 4444 > <file>`

### Send

`nc -nv <target ip> 4444 < <file>`

## Powercat

### Send

```
powercat -c <ip> -p 443 -i <file>
```

## Powershell

```
powershell -c "(new-object System.Net.WebClient).DownloadFile('http://<ip>/<file>','C:\Users\<user>\Desktop\<file>')"
```

## socat

### Serve

`sudo socat TCP4-LISTEN:443,fork file:<file>`

### Client

`socat TCP4:<server ip>:443 file:<file>,create`

