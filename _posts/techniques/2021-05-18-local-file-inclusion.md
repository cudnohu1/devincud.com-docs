---
title: Local File Inclusion
author: Devin Cudnohufsky
date: 2021-05-18 00:00:00 +0500
categories: [Techniques]
tags: [php]
---

## PHP

`php://filter/convert.base64-encode/resource=<php file>`
