---
title: Netcat
author: Devin Cudnohufsky
date: 2022-01-19 00:00:00 +0500
categories: [Tools ]
tags: [nc]
---

## Client Mode

`nc -nv <ip>`

1. -n: Do not resolve DNS entries
1. -v: Add verbosity

## Server Mode

`nc -nvlp 4444`

1. -n: Do not resolve DNS entries
1. -v: Add verbosity
1. -l: Listen Mode
1. -p: Specify port to listen on
