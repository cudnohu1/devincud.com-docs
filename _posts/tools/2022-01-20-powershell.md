---
title: Powershell
author: Devin Cudnohufsky
date: 2022-01-12 00:00:00 +0500
categories: [Tools ]
tags: [powershell]
---

## Execution Policy

### All Users

```
Set-ExecutionPolicy Unrestricted
```

### Current User (Non-Admin)

```
Set-ExecutionPolicy -Scope CurrentUser Unrestricted
```
