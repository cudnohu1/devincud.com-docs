---
title: OverTheWire - Bandit
author: Devin Cudnohufsky
date: 2020-04-08 00:00:00 +0500
categories: [Write-Ups]
tags: [bash]
---

## Level 0

Password (given): `bandit0`

```
ssh bandit0@bandit.labs.overthewire.org -p 2220
ls -al
cat readme
```

## Level 1

Password: `boJ9jbbUNNfktd78OOpsqOltutMc3MY1`

```
ssh bandit1@bandit.labs.overthewire.org -p 2220
ls -al
cat ./-
```

## Level 2

Password: `CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9`

```
ssh bandit2@bandit.labs.overthewire.org -p 2220
ls -al
cat "spaces in this filename"
```

## Level 3

Password: `UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK`

```
ssh bandit3@bandit.labs.overthewire.org -p 2220
ls -al
cd inhere
ls -al
cat .hidden
```

## Level 4

Password: `pIwrPrtPN36QITSp3EQaw936yaFoFgAB`

```
ssh bandit4@bandit.labs.overthewire.org -p 2220
ls -al
cd inhere
ls -al
for f in *; do cat ./$f; echo; done
```

## Level 5

Password: `koReBOKuIDDepwhWk7jZC0RTdopnAYKh`

Instructions:

- human-readable
- 1033 bytes in size
- not executable

```
ssh bandit5@bandit.labs.overthewire.org -p 2220
ls -al
cd inhere
ls -al
find . -type f -size 1033c ! -executable
cat ./maybehere07/.file2
```

## Level 6

Password: `DXjZPULLxYr17uwoI01bNLQbtFemEgo7`

Instructions:

- owned by user bandit7
- owned by group bandit6
- 33 bytes in size

```
ssh bandit6@bandit.labs.overthewire.org -p 2220
find / -type f -size 33c -group bandit6 -user bandit7
cat /var/lib/dpkg/info/bandit7.password
```

## Level 7

Password: `HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs`

Instructions:

The password for the next level is stored in the file **data.txt** next to the word **millionth**

```
ssh bandi7@bandit.labs.overthewire.org -p 2220
grep millionth data.txt
```

## Level 8

Password: `cvX2JJa4CFALtqS87jk27qwqGhBM9plV`

Instructions:

The password for the next level is stored in the file data.txt and is the only line of text that occurs only once

```
ssh bandi8@bandit.labs.overthewire.org -p 2220
cat data.txt | sort | uniq -c
```

## Level 9

Password: `UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR`

Instructions:

The password for the next level is stored in the file data.txt in one of the few human-readable strings, beginning with several ‘=’ characters.

```
ssh bandi9@bandit.labs.overthewire.org -p 2220
strings data.txt | grep ====
```

## Level 10

Password: `truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk`

Instructions:

The password for the next level is stored in the file data.txt, which contains base64 encoded data

```
ssh bandi10@bandit.labs.overthewire.org -p 2220
base64 -d data.txt
```

## Level 11

Password: `IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR`

Instructions:

The password for the next level is stored in the file data.txt, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions

```
ssh bandi11@bandit.labs.overthewire.org -p 2220
cat data.txt | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'
```

## Level 12

Password: `5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu`

Instructions:

The password for the next level is stored in the file data.txt, which is a hexdump of a file that has been repeatedly compressed. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!)

```
ssh bandit12@bandit.labs.overthewire.org -p 2220
xxd -r data.txt | file -
xxd -r data.txt | zcat | file -
xxd -r data.txt | zcat | bzcat | file -
xxd -r data.txt | zcat | bzcat | zcat | file -
xxd -r data.txt | zcat | bzcat | zcat | tar xO | file -
xxd -r data.txt | zcat | bzcat | zcat | tar xO | tar xO | file -
xxd -r data.txt | zcat | bzcat | zcat | tar xO | tar xO | bzcat | file -
xxd -r data.txt | zcat | bzcat | zcat | tar xO | tar xO | bzcat | tar xO | file -
xxd -r data.txt | zcat | bzcat | zcat | tar xO | tar xO | bzcat | tar xO | zcat | file -
xxd -r data.txt | zcat | bzcat | zcat | tar xO | tar xO | bzcat | tar xO | zcat
```

## Level 13

Password: `8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL`

Instructions:

The password for the next level is stored in /etc/bandit_pass/bandit14 and can only be read by user bandit14. For this level, you don’t get the next password, but you get a private SSH key that can be used to log into the next level. Note: localhost is a hostname that refers to the machine you are working on

```
ssh bandit13@bandit.labs.overthewire.org -p 2220
ls -a
cat sshkey.private
ssh bandit14@localhost -i sshkey.private
cat /etc/bandit_pass/bandit14
```

## Level 14

Password: `4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e`

Instructions:

The password for the next level can be retrieved by submitting the password of the current level to port 30000 on localhost.

```
ssh bandit14@bandit.labs.overthewire.org -p 2220
echo "4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e" | nc localhost 30000
```

## Level 15

Password: `BfMYroe26WYalil77FoDi9qh59eK5xNr`

Instructions:

The password for the next level can be retrieved by submitting the password of the current level to port 30001 on localhost using SSL encryption.

Helpful note: Getting “HEARTBEATING” and “Read R BLOCK”? Use -ign_eof and read the “CONNECTED COMMANDS” section in the manpage. Next to ‘R’ and ‘Q’, the ‘B’ command also works in this version of that command…

```
ssh bandit15@bandit.labs.overthewire.org -p 2220
(echo "BfMYroe26WYalil77FoDi9qh59eK5xNr"; sleep 2;) | openssl s_client -connect localhost:30001
```

## Level 16

Password: `cluFn7wTiGryunymYOu4RcffSxQluehd`

Instructions:

The credentials for the next level can be retrieved by submitting the password of the current level to a port on localhost in the range 31000 to 32000. First find out which of these ports have a server listening on them. Then find out which of those speak SSL and which don’t. There is only 1 server that will give the next credentials, the others will simply send back to you whatever you send to it.

```
ssh bandit16@bandit.labs.overthewire.org -p 2220
nmap -p 31000-32000 localhost
(echo "cluFn7wTiGryunymYOu4RcffSxQluehd"; sleep 2;) | openssl s_client -connect localhost:31790
mkdir /tmp/cudnohu1_1
echo "-----BEGIN RSA PRIVATE KEY-----
> MIIEogIBAAKCAQEAvmOkuifmMg6HL2YPIOjon6iWfbp7c3jx34YkYWqUH57SUdyJ
> imZzeyGC0gtZPGujUSxiJSWI/oTqexh+cAMTSMlOJf7+BrJObArnxd9Y7YT2bRPQ
> Ja6Lzb558YW3FZl87ORiO+rW4LCDCNd2lUvLE/GL2GWyuKN0K5iCd5TbtJzEkQTu
> DSt2mcNn4rhAL+JFr56o4T6z8WWAW18BR6yGrMq7Q/kALHYW3OekePQAzL0VUYbW
> JGTi65CxbCnzc/w4+mqQyvmzpWtMAzJTzAzQxNbkR2MBGySxDLrjg0LWN6sK7wNX
> x0YVztz/zbIkPjfkU1jHS+9EbVNj+D1XFOJuaQIDAQABAoIBABagpxpM1aoLWfvD
> KHcj10nqcoBc4oE11aFYQwik7xfW+24pRNuDE6SFthOar69jp5RlLwD1NhPx3iBl
> J9nOM8OJ0VToum43UOS8YxF8WwhXriYGnc1sskbwpXOUDc9uX4+UESzH22P29ovd
> d8WErY0gPxun8pbJLmxkAtWNhpMvfe0050vk9TL5wqbu9AlbssgTcCXkMQnPw9nC
> YNN6DDP2lbcBrvgT9YCNL6C+ZKufD52yOQ9qOkwFTEQpjtF4uNtJom+asvlpmS8A
> vLY9r60wYSvmZhNqBUrj7lyCtXMIu1kkd4w7F77k+DjHoAXyxcUp1DGL51sOmama
> +TOWWgECgYEA8JtPxP0GRJ+IQkX262jM3dEIkza8ky5moIwUqYdsx0NxHgRRhORT
> 8c8hAuRBb2G82so8vUHk/fur85OEfc9TncnCY2crpoqsghifKLxrLgtT+qDpfZnx
> SatLdt8GfQ85yA7hnWWJ2MxF3NaeSDm75Lsm+tBbAiyc9P2jGRNtMSkCgYEAypHd
> HCctNi/FwjulhttFx/rHYKhLidZDFYeiE/v45bN4yFm8x7R/b0iE7KaszX+Exdvt
> SghaTdcG0Knyw1bpJVyusavPzpaJMjdJ6tcFhVAbAjm7enCIvGCSx+X3l5SiWg0A
> R57hJglezIiVjv3aGwHwvlZvtszK6zV6oXFAu0ECgYAbjo46T4hyP5tJi93V5HDi
> Ttiek7xRVxUl+iU7rWkGAXFpMLFteQEsRr7PJ/lemmEY5eTDAFMLy9FL2m9oQWCg
> R8VdwSk8r9FGLS+9aKcV5PI/WEKlwgXinB3OhYimtiG2Cg5JCqIZFHxD6MjEGOiu
> L8ktHMPvodBwNsSBULpG0QKBgBAplTfC1HOnWiMGOU3KPwYWt0O6CdTkmJOmL8Ni
> blh9elyZ9FsGxsgtRBXRsqXuz7wtsQAgLHxbdLq/ZJQ7YfzOKU4ZxEnabvXnvWkU
> YOdjHdSOoKvDQNWu6ucyLRAWFuISeXw9a/9p7ftpxm0TSgyvmfLF2MIAEwyzRqaM
> 77pBAoGAMmjmIJdjp+Ez8duyn3ieo36yrttF5NSsJLAbxFpdlc1gvtGCWW+9Cq0b
> dxviW8+TFVEBl1O4f7HVm6EpTscdDxU+bCXWkfjuRb7Dy9GOtt9JPsX8MBTakzh3
> vBgsyi/sN3RqRBcGU40fOoZyfAMT8s1m/uYv52O6IgeuZ/ujbjY=
> -----END RSA PRIVATE KEY-----" > /tmp/cudnohu1_1/bandit17.key
chmod 700 /tmp/cudnohu1_1/badit17.key
ssh -i /tmp/cudnohu1_1/bandit17.key bandit17@localhost
cat /etc/bandit_pass/bandit17
```

## Level 17

Password: `xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn`

Instructions:

There are 2 files in the homedirectory: passwords.old and passwords.new. The password for the next level is in passwords.new and is the only line that has been changed between passwords.old and passwords.new

```
ssh bandit17@bandit.labs.overthewire.org -p 2220
diff passwords.old passwords.new
```

## Level 18

Password: `kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd`

Instructions:

The password for the next level is stored in a file readme in the homedirectory. Unfortunately, someone has modified .bashrc to log you out when you log in with SSH.

```
ssh bandit18@bandit.labs.overthewire.org -p 2220 cat readme
```

## Level 19

Password: `IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x`

Instructions:

To gain access to the next level, you should use the setuid binary in the homedirectory. Execute it without arguments to find out how to use it. The password for this level can be found in the usual place (/etc/bandit_pass), after you have used the setuid binary.

```
ssh bandit19@bandit.labs.overthewire.org -p 2220
./bandit20-do cat /etc/bandit_pass/bandit20
```

## Level 20

Password: `GbKksEFF4yrVs6il55v6gwY5aVje5f0j`

Instructions:

There is a setuid binary in the homedirectory that does the following: it makes a connection to localhost on the port you specify as a commandline argument. It then reads a line of text from the connection and compares it to the password in the previous level (bandit20). If the password is correct, it will transmit the password for the next level (bandit21).

```
ssh bandit20@bandit.labs.overthewire.org -p 2220
echo "GbKksEFF4yrVs6il55v6gwY5aVje5f0j" | nc -l localhost -p 12345 &
./suconnect 12345
```

## Level 21

Password: `gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr`

Instructions:

A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.

```
ssh bandit21@bandit.labs.overthewire.org -p 2220
cd /etc/cron.d/
ls -al
cat cronjob_bandit22
cat /usr/bin/cronjob_bandit22.sh
/tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
```

## Level 22

Password: `Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI`

Instructions:

A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.

```
ssh bandit22@bandit.labs.overthewire.org -p 2220
cat /etc/cron.d/cronjob_bandit23
cat /usr/bin/cronjob_bandit23.sh
echo "I am user bandit23" | md5sum | cut -d ' ' -f 1
cat /tmp/8ca319486bfbbc3663ea0fbe81326349
```

## Level 23

Password: `jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n`

Instructions:

A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.

```
ssh bandit23@bandit.labs.overthewire.org -p 2220
cat /etc/cron.d/cronjob_bandit24
cat /usr/bin/cronjob_bandit24.sh
mkdir /tmp/cudnohu1_2
echo "#!/bin/bash
cat /etc/bandit_pass/bandit24 > /tmp/cudnohu1_2/bandit24" > /tmp/cudnohu1_2/grabthepass.sh
touch bandit24
chmod 777 /tmp/cudnohu1_2/*
cp /tmp/cudnohu1_2/grabthepass.sh /var/spool/bandit24/
sleep 60 && cat /tmp/cudnohu1_2/bandit24
```

## Level 24

Password: `UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ`

Instructions:

A daemon is listening on port 30002 and will give you the password for bandit25 if given the password for bandit24 and a secret numeric 4-digit pincode. There is no way to retrieve the pincode except by going through all of the 10000 combinations, called brute-forcing.

```
ssh bandit24@bandit.labs.overthewire.org -p 2220
mkdir /tmp/cudnohu1_3
cd /tmp/cudnohu1_3
vi grabthepass.sh
chmod +x grabthepass.sh
./grabthepass.sh
```

```
#!/bin/bash

for i in {0..9}
do
	> /tmp/cudnohu1_3/passlist

	for j in {000..999}
	do
		echo "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ $i$j" >> /tmp/cudnohu1_3/passlist
	done

	cat /tmp/cudnohu1_3/passlist | nc -w 5 localhost 30002 | grep -v "Wrong"

done
```

> Note this required being broken apart into sections due to hitting the timeout on connections

## Level 25

Password: `uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG`

Instructions:

Logging in to bandit26 from bandit25 should be fairly easy… The shell for user bandit26 is not /bin/bash, but something else. Find out what it is, how it works and how to break out of it.

```
ssh bandit25@bandit.labs.overthewire.org -p 2220
ls -al
cat bandit26.sshkey
grep bandit26 /etc/passwd
cat /usr/bin/showtext
ssh bandit26@localhost -o StrictHostKeyChecking=no -i ~/bandit26.sshkey
v
:e /etc/bandit_pass/bandit26
```

> *Note:* The terminal window needs to be of minimal height for the more operation when sshing to user bandit26. If the terminal is too large, the file will just display on the screen and exit.

## Level 26

Password: `5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z`

Instructions:

Good job getting a shell! Now hurry and grab the password for bandit27!

```
ssh bandit26@bandit.labs.overthewire.org -p 2220
v
:set shell=/bin/bash
:shell
ls -al
./bandit27-do cat /etc/bandit_pass/bandit27
```

> *Note:* You will need to use the window resizing trick from the previous level here as well.

## Level 27

Password: `3ba3118a22e93127a4ed485be72ef5ea`

Instructions:

There is a git repository at ssh://bandit27-git@localhost/home/bandit27-git/repo. The password for the user bandit27-git is the same as for the user bandit27.

Clone the repository and find the password for the next level.

```
ssh bandit27@bandit.labs.overthewire.org -p 2220
mkdir /tmp/cudnohu1_4
cd /tmp/cudnohu1_4
git clone ssh://bandit27-git@localhost/home/bandit27-git/repo
cd repo
ls -al
cat README
```

## Level 28

Password: `0ef186ac70e04ea33b4c1853d2526fa2`

Instructions:

There is a git repository at ssh://bandit28-git@localhost/home/bandit28-git/repo. The password for the user bandit28-git is the same as for the user bandit28.

Clone the repository and find the password for the next level.

```
ssh bandit28@bandit.labs.overthewire.org -p 2220
mkdir /tmp/cudnohu1_5
cd /tmp/cudnohu1_5
git clone ssh://bandit28-git@localhost/home/bandit28-git/repo
cd repo
ls -al
git log -p README
```

## Level 29

Password: `bbc96594b4e001778eee9975372716b2`

Instructions:

There is a git repository at ssh://bandit29-git@localhost/home/bandit29-git/repo. The password for the user bandit29-git is the same as for the user bandit29.

Clone the repository and find the password for the next level.

```
ssh bandit29@bandit.labs.overthewire.org -p 2220
mkdir /tmp/cudnohu1_6
cd /tmp/cudnohu1_6
git clone ssh://bandit29-git@localhost/home/bandit29-git/repo
cd repo
ls -al
git log -p README
git branch
git branch -r
git checkout dev
cat README
```

## Level 30

Password: `5b90576bedb2cc04c86a9e924ce42faf`


Instructions:

There is a git repository at ssh://bandit30-git@localhost/home/bandit30-git/repo. The password for the user bandit30-git is the same as for the user bandit30.

Clone the repository and find the password for the next level.

```
ssh bandit30@bandit.labs.overthewire.org -p 2220
mkdir /tmp/cudnohu1_7
cd /tmp/cudnohu1_7
git clone ssh://bandit30-git@localhost/home/bandit30-git/repo
cd repo
ls -al
cat README
git log -p README
git branch
git branch -r
git tag
git show secret
```

## Level 31

Password: `47e603bb428404d265f59c42920d81e5`

Instructions:

There is a git repository at ssh://bandit31-git@localhost/home/bandit31-git/repo. The password for the user bandit31-git is the same as for the user bandit31.

Clone the repository and find the password for the next level.

```
ssh bandit31@bandit.labs.overthewire.org -p 2220
mkdir /tmp/cudnohu1_8
cd /tmp/cudnohu1_8
git clone ssh://bandit31-git@localhost/home/bandit31-git/repo
cd repo
ls -al
cat README
cat .gitignore
rm .gitignore
echo 'May I come in?' > key.txt
git add key.txt
git commit -a
git push
```

## Level 32

Password: `56a9bf19c63d650ce78e6ec0354ee45e`

Instructions:

After all this git stuff its time for another escape. Good luck!

```
ssh bandit32@bandit.labs.overthewire.org -p 2220
$0
cat /etc/bandit_pass/bandit33
```

## Level 33

Password: `c9c3199ddf4121b10cf581a98d51caee`

Instructions:

At this moment, level 34 does not exist yet.
