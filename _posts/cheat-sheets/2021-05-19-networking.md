---
title: Networking
author: Devin Cudnohufsky
date: 2021-05-19 00:00:00 +0500
categories: [Cheat-Sheets]
tags: [ports]
---

## Common Ports

| Port     | Service           |
|----------|-------------------|
| 21/tcp   | FTP               |
| 22/tcp   | SSH               |
| 23/tcp   | Telnet            |
| 25/tcp   | SMTP              |
| 49/tcp   | TACACS            |
| 53/udp   | DNS               |
| 67/udp   | DHCP              |
| 68/udp   | DHCP              |
| 69/udp   | TFTP              |
| 80/tcp   | HTTP              |
| 88/tcp   | Kerberos          |
| 110/tcp  | POP3              |
| 111/tcp  | RPC               |
| 123/udp  | NTP               |
| 135/tcp  | Windows RPC       |
| 137/tcp  | NetBIOS           |
| 138/tcp  | NetBIOS           |
| 139/tcp  | SMB               |
| 143/tcp  | IMAP              |
| 161/udp  | SNMP              |
| 179/tcp  | BGP               |
| 201/tcp  | AppleTalk         |
| 389/tcp  | LDAP              |
| 443/tcp  | HTTPS             |
| 445/tcp  | SMB               |
| 500/udp  | ISAKMP            |
| 514/tcp  | Syslog            |
| 520/tcp  | RIP               |
| 546/tcp  | DHCPv6            |
| 547/tcp  | DHCPv6            |
| 587/tcp  | SMTP              |
| 902/tcp  | VMWare            |
| 1080/tcp | Socks Proxy       |
| 1194/tcp | VPN               |
| 1433/tcp | MS-SQL            |
| 1434/tcp | MS-SQL            |
| 1521/tcp | Oracle            |
| 1629/tcp | DameWare          |
| 2049/tcp | NFS               |
| 3128/tcp | Squid Proxy       |
| 3306/tcp | MySQL             |
| 3389/tcp | RDP               |
| 5060/tcp | SIP               |
| 5222/tcp | Jabber            |
| 5432/tcp | Postgres          |
| 5666/tcp | Nagios            |
| 5900/tcp | VNC               |
| 6000/tcp | X11               |
| 6129/tcp | DameWare          |
| 6667/tcp | IRC               |
| 9001/tcp | Tor               |
| 9001/tcp | HSQL              |
| 9090/tcp | Openfire          |
| 9091/tcp | Openfire          |
| 9100/tcp | Jet Direct        |

## CIDR

| CIDR     | Mask              | Host Count |
|----------|-------------------|------------|
| /31      | 255.255.255.254   | 1          |
| /30      | 255.255.255.252   | 2          |
| /29      | 255.255.255.248   | 6          |
| /28      | 255.255.255.240   | 14         |
| /27      | 255.255.255.224   | 30         |
| /26      | 255.255.255.192   | 62         |
| /25      | 255.255.255.128   | 126        |
| /24      | 255.255.255.0     | 254        |
| /23      | 255.255.254.0     | 510        |
| /22      | 255.255.252.0     | 1022       |
| /21      | 255.255.248.0     | 2046       |
| /20      | 255.255.240.0     | 4094       |
| /19      | 255.255.224.0     | 8190       |
| /18      | 255.255.192.0     | 16382      |
| /17      | 255.255.128.0     | 32766      |
| /16      | 255.255.0.0       | 65534      |
| /15      | 255.254.0.0       | 131070     |
| /14      | 255.252.0.0       | 262142     |
| /13      | 255.248.0.0       | 524286     |
| /12      | 255.240.0.0       | 1048574    |
| /11      | 255.224.0.0       | 2097150    |
| /10      | 255.192.0.0       | 4194302    |
| /9       | 255.128.0.0       | 8388606    |
| /8       | 255.0.0.0         | 16777214   |

## TTL Fingerprinting

| TTL      | OS                |
|----------|-------------------|
| 128      | Windows           |
| 64       | Linux             |
| 255      | Network           |
| 255      | Solaris           |