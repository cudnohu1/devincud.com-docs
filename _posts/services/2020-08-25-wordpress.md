---
title: Wordpress
author: Devin Cudnohufsky
date: 2020-08-25 00:00:00 +0500
categories: [Services]
tags: [wpscan]
---

## Vulnerability Scan

`wpscan -u <url>`

## Enumerate Users

`wpscan -u <url> --enumerate u`